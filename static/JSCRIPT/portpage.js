
/*
const addEntryButton = document.getElementById('add-entry-button');
const addEntryModal = document.getElementById('add-entry-modal');

addEntryButton.addEventListener('click', () => {
    addEntryModal.style.display = 'block';
});

// Close the modal
function closeAddEntryModal() {
    addEntryModal.style.display = 'none';
}

// Close the modal if the user clicks outside the modal content
window.addEventListener('click', (event) => {
    if (event.target === addEntryModal) {
        closeAddEntryModal();
    }
});

const entryForm = document.getElementById('entry-form');

entryForm.addEventListener('submit', (event) => {
    event.preventDefault();

    // Add logic here to process the form data and add a new entry
    // You can use AJAX requests or other methods to save the data

    // After successfully adding the entry, close the modal
    closeAddEntryModal();
});
*/

class TableModal {
    constructor() {
        this.tableRows = document.querySelectorAll(".table-row");
        this.modal = document.getElementById("myModal");
        this.modalContent = document.getElementById("modal-content");
        this.closeModalButton = document.getElementsByClassName("close")[0];
        this.modalButton = document.getElementById("modal-button");
        this.modalButtonClickListener = this.modalButtonClickListener.bind(this); // Bind the listener to the instance
        this.attachEventListeners();
    }



    displayModal(row) {
        // Extract data from the row
        const dataCells = row.querySelectorAll
            ("td:nth-child(1)," +
            "td:nth-child(3)," +
            "td:nth-child(4)," +
            "td:nth-child(5)," +
            "td:nth-child(6)," +
            "td:nth-child(7)," +
            "td:nth-child(8)," +
            "td:nth-child(9)");
        const [id, type, name, code, icao, customsPortCode, countryCode, timeZoneCode] = dataCells;

        // Create the message
        const message = `
            Id: ${id.textContent}<br>
            Type: ${type.textContent}<br>
            Name of Port: ${name.textContent}<br>
            IATA Code: ${code.textContent}<br>
            ICAO: ${icao.textContent}<br>
            Custom Port Code: ${customsPortCode.textContent}<br>
            Country Code: ${countryCode.textContent}<br>
            Time Zone: ${timeZoneCode.textContent}<br>
        `;

        this.modalContent.innerHTML = message;
        this.modalButton.dataset.rowId = dataCells[0].textContent;
        this.modal.style.display = "block";
        this.modalButton.addEventListener("click", this.modalButtonClickListener);
    }

    modalButtonClickListener() {
        // Get the ID from the data attribute
        const id = this.modalButton.dataset.rowId;

        // Remove the listener to prevent multiple clicks
        this.modalButton.removeEventListener("click", this.modalButtonClickListener);
        this.viewPortData(id);
    }

    viewPortData(id) {

        const idQuerry = id

        fetch(`/port-view?id=${idQuerry}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error("Error updating data");
                }
            })
            .then((data) => {
                if (data.text) {
                    const textValue = data.text;
                    console.log("Text from the backend:", textValue);
                } else {
                    console.error("Text not found in the response data.");
                }
            })
            .catch((error) => {
                console.error("Error:", error);
                alert(error.message);
            });
        console.log("Data: ", idQuerry);
    }


    attachEventListeners() {
        this.tableRows.forEach((row) => {
            row.addEventListener("click", () => {
                this.displayModal(row);
            });
        });


        this.closeModalButton.onclick = () => {
            this.modalButton.removeEventListener("click", this.modalButtonClickListener);
            this.modal.style.display = "none";
        };

        window.onclick = (event) => {
            if (event.target == this.modal) {
                this.modalButton.removeEventListener("click", this.modalButtonClickListener);
                this.modal.style.display = "none";
            }
        };
    }
}

document.addEventListener("DOMContentLoaded", () => {
    new TableModal();
});
