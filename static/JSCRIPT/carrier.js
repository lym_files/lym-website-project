

class TableModal {
    constructor() {
        this.tableRows = document.querySelectorAll(".table-row");
        this.modal = document.getElementById("myModal");
        this.modalContent = document.getElementById("modal-content");
        this.closeModalButton = document.getElementsByClassName("close")[0];

        this.attachEventListeners();
    }

    displayModal(row) {
        const PCode = row.querySelector("td:nth-child(2)").textContent;
        const CCode = row.querySelector("td:nth-child(3)").textContent;
        const vessel = row.querySelector("td:nth-child(4)").textContent;
        const en = row.querySelector("td:nth-child(5)").textContent;
        const cn = row.querySelector("td:nth-child(6)").textContent;
        const transportT = row.querySelector("td:nth-child(7)").textContent;
        const IATA = row.querySelector("td:nth-child(8)").textContent;
        const SS = row.querySelector("td:nth-child(9)").textContent;
        const FS = row.querySelector("td:nth-child(10)").textContent;
        const FC = row.querySelector("td:nth-child(11)").textContent;

        const message = `
            3 Digit Prefix Code: ${PCode}<br>
            Carrier Code: ${CCode}<br>
            Vessel Flag: ${vessel}<br>
            En Name: ${en}<br>
            Cn Name: ${cn}<br>
            Transportation Type: ${transportT}<br>
            IATA Code: ${IATA}<br>
            Security Surcharge Fee: ${SS}<br>
            Fuel Surcharge Fee: ${FS}<br>
            Firms Code: ${FC}<br>
        `;

        this.modalContent.innerHTML = message;
        this.modal.style.display = "block";
    }

    attachEventListeners() {
        this.tableRows.forEach((row) => {
            row.addEventListener("click", () => {
                this.displayModal(row);
            });
        });

        this.closeModalButton.onclick = () => {
            this.modal.style.display = "none";
        };

        window.onclick = (event) => {
            if (event.target == this.modal) {
                this.modal.style.display = "none";
            }
        };
    }
}

// Initialize the TableModal class when the DOM is ready
document.addEventListener("DOMContentLoaded", () => {
    new TableModal();
});
