

function openTab(tabName) {
    var i, tabContents, tabLinks;
    tabContents = document.getElementsByClassName("tab-content");
    for (i = 0; i < tabContents.length; i++) {
        tabContents[i].style.display = "none";
    }
    tabLinks = document.getElementsByClassName("tab-link");
    for (i = 0; i < tabLinks.length; i++) {
        tabLinks[i].classList.remove("active");
    }
    document.getElementById(tabName).style.display = "block";
    event.currentTarget.classList.add("active");
}

// Attach click event listeners to tab links
var tabLinks = document.querySelectorAll(".tab-link");
tabLinks.forEach(function (tabLink) {
    tabLink.addEventListener("click", function () {
        openTab(this.getAttribute("data-tab"));
    });
});
