package controller

import (
	"database/sql"
	"log"
	"net/http"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
)

type Carrier struct {
	Code         string
	Carrier_code string
	Vessel_flag  string
	En_name      string
	Cn_name      string
	Type         string
	IATA_number  string
	Security_fee string
	Oil_fee      string
	Customs_code string
}

/*
func getSQLiteDB() *sql.DB {

	var err error
	globalSQLiteDBAttend, err = sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		panic(err)
	}

	err = globalSQLiteDBAttend.Ping()
	if err != nil {
		panic(err)
	}

	return globalSQLiteDBAttend
}
*/

func CarrierInfo(w http.ResponseWriter, r *http.Request) {
	//	fmt.Println("Probing")

	db := getSQLDB()

	selectQuery := "SELECT Code, Carrier_code, Vessel_flag, En_name, Cn_name, Type, IATA_number, Security_fee, Oil_fee, Customs_code FROM base_air_line"
	rows, err := db.Query(selectQuery)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var views []Carrier

	for rows.Next() {
		var view Carrier
		var (
			NullCode,
			NullCarrier_code,
			NullVessel_flag,
			NullEn_name,
			NullCn_name,
			NullType,
			NullIATA_number,
			NullSecurity_fee,
			NullOil_fee,
			NullCustoms_code sql.NullString
		)
		err := rows.Scan(&NullCode,
			&NullCarrier_code,
			&NullVessel_flag,
			&NullEn_name,
			&NullCn_name,
			&NullType,
			&NullIATA_number,
			&NullSecurity_fee,
			&NullOil_fee,
			&NullCustoms_code)
		if err != nil {
			log.Fatal(err)
		}

		if NullCode.Valid {
			view.Code = NullCode.String
		}
		if NullCarrier_code.Valid {
			view.Carrier_code = NullCarrier_code.String
		}
		if NullVessel_flag.Valid {
			view.Vessel_flag = NullVessel_flag.String
		}
		if NullEn_name.Valid {
			view.En_name = NullEn_name.String
		}
		if NullCn_name.Valid {
			view.Cn_name = NullCn_name.String
		}
		if NullType.Valid {
			view.Type = NullType.String
		}
		if NullIATA_number.Valid {
			view.IATA_number = NullIATA_number.String
		}
		if NullSecurity_fee.Valid {
			view.Security_fee = NullSecurity_fee.String
		}
		if NullOil_fee.Valid {
			view.Oil_fee = NullOil_fee.String
		}
		if NullCustoms_code.Valid {
			view.Customs_code = NullCustoms_code.String
		}

		views = append(views, view)
	}

	tmpl, err := template.ParseFiles("static/HTML/Carrier_page.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, views)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
