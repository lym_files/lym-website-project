package controller

import (
	"encoding/base32"
	"encoding/base64"
	"fmt"
	otp2 "github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/skip2/go-qrcode"
	"log"
	"net/http"
	"net/smtp"
	"text/template"
	"time"
)

func BasicPage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Probing")

	tmpl, err := template.ParseFiles("static/HTML/Basic_page.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func GenerateOTP(w http.ResponseWriter, r *http.Request) {
	// Get the secret key from the query parameters
	//encodedStr := r.URL.Query().Get("secret")

	//encodedStr := "44YSXNEZRJXVBZLUXOXFUMRBR3CT4EPF"
	encodedStr := "GMXAE3INFQMTQT6XU5DUO4FFLXESN25Q"

	// Decode the secret key
	decodedSecretKey, err := base32.StdEncoding.DecodeString(encodedStr)
	if err != nil {
		http.Error(w, "Error decoding secret key", http.StatusBadRequest)
		return
	}

	issuer := "NetChb"
	accountName := "3790LaxBW"

	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      issuer,
		AccountName: accountName,
		Secret:      decodedSecretKey,
		Period:      30,
		Digits:      6,
		Algorithm:   otp2.AlgorithmSHA1,
	})
	if err != nil {
		http.Error(w, "Error generating TOTP", http.StatusInternalServerError)
		return
	}

	// Generate the OTP
	otp, err := totp.GenerateCode(key.Secret(), time.Now())
	if err != nil {
		http.Error(w, "Error generating OTP", http.StatusInternalServerError)
		return
	}

	// Generate the OTP remaining time (time until the next OTP)
	//timeRemaining := key.Digits()

	//timeRemaining := key.

	otpURI := key.URL()

	// Generate and return a QR code
	qrCode, err := qrcode.New(otpURI, qrcode.Medium)
	if err != nil {
		http.Error(w, "Error generating QR code", http.StatusInternalServerError)
		return
	}

	// Encode the QR code as a PNG and write it to the response
	qrCode.WriteFile(256, "generaOTP2.png")

	qrCodeBytes, err := qrCode.PNG(256)
	if err != nil {
		http.Error(w, "Error generating QR code", http.StatusInternalServerError)
		return
	}
	qrCodeBase64 := base64.StdEncoding.EncodeToString(qrCodeBytes)

	// Generate and return the OTP and time remaining as JSON
	//fmt.Fprintf(w, `{"otp":"%s","timeRemaining":%d}`, otp, timeRemaining)
	//fmt.Fprintf(w, `"OTP": "%s", "Issuer": "%v", AccountName": "%v"`, otp, issuer, accountName)

	/// Send to Email

	emailBody := fmt.Sprintf("Issuer: %s\r\nAccountName: %s\r\nOTP: %s\r\n", issuer, accountName, otp)

	email := "virgodosal@gmail.com"
	password := "lbsx oywn wnkq huwv"

	smtpServer := "smtp.gmail.com"
	smtpPort := "587"

	//to := []string{"it.mnlph@gmail.com"}
	to := []string{"virgodosal@gmail.com"}

	msg := []byte("To: virgodosal@gmail.com\r\n" +
		"Subject: Testing\r\n" +
		"\r\n" + emailBody) // Use the emailBody variable here

	auth := smtp.PlainAuth("", email, password, smtpServer)
	err = smtp.SendMail(smtpServer+":"+smtpPort, auth, email, to, msg)

	if err != nil {
		log.Fatal(err)
	} else {
		log.Println("Email sent successfully!")
	}

	data := struct {
		Issuer      string
		AccountName string
		OTP         string
		QR          string
	}{
		Issuer:      issuer,
		AccountName: accountName,
		OTP:         otp,
		QR:          qrCodeBase64,
	}

	fmt.Printf("Issuer: %v, AccountName: %v, OTP: %v, QR: %v", issuer, accountName, otp, qrCodeBase64)

	// Execute the HTML template
	tmpl, err := template.ParseFiles("static/HTML/OTP_page.html")
	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

}
