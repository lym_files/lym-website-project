package controller

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
)

type AWB_data struct {
	Name        string
	Customer_Id string
	Type        string
	Company     string
	Address1    string
	State       string
}

func AWB_Address_Info(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Probing here")

	db := getSQLDB()

	selectQuery := "SELECT Name, Customer_Id, Type, Company, Address1, State FROM customs_awb_address"
	rows, err := db.Query(selectQuery)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var views []AWB_data

	for rows.Next() {
		var view AWB_data
		var (
			nameNull, customer_IdNull, typeNull, companyNull, address1Null, stateNull sql.NullString
		)
		err := rows.Scan(&nameNull, &customer_IdNull, &typeNull, &companyNull, &address1Null, &stateNull)
		if err != nil {
			log.Fatal(err)
		}
		// Check if the value is NULL and handle it accordingly
		if nameNull.Valid {
			view.Name = nameNull.String
		}
		if customer_IdNull.Valid {
			view.Customer_Id = customer_IdNull.String
		}
		if typeNull.Valid {
			view.Type = typeNull.String
		}
		if companyNull.Valid {
			view.Company = companyNull.String
		}
		if address1Null.Valid {
			view.Address1 = address1Null.String
		}
		if stateNull.Valid {
			view.State = stateNull.String
		}

		views = append(views, view)
	}

	tmpl, err := template.ParseFiles("static/HTML/Awb_Address_page.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, views)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
