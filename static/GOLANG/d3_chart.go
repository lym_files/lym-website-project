package controller

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
)

func D3ChartInfo(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Probing d3 sample")

	tmpl, err := template.ParseFiles("static/HTML/D3_SAMPLE_PAGE.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
