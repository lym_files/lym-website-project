package controller

import "database/sql"

type Consignee struct {
	ID           int
	UserId       int
	Name         string
	Mode         string
	AttendedDate string
	AttendedTime string
	Method       string
}

func getSQLiteDB3() *sql.DB {

	var err error
	globalSQLiteDBAttend, err = sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		panic(err)
	}

	err = globalSQLiteDBAttend.Ping()
	if err != nil {
		panic(err)
	}

	return globalSQLiteDBAttend
}
