package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
)

type PortData struct {
	Ids               string
	Type              string
	Name              string
	Code              string
	ICAO              string
	Customs_Port_Code string
	Country_Code      string
	Time_Zone_Code    string
}

type PortEntryData struct {
	ID              string `json:"id"`
	Type            string `json:"type"`
	Name            string `json:"name"`
	Iata            string `json:"iata"`
	CreateUser      string
	UpdateUser      string
	Icao            string `json:"icao"`
	CustomsPortCode string `json:"customsPortCode"`
	CountryCode     string `json:"countryCode"`
	TimeZoneCode    string `json:"timeZoneCode"`
}

type PortViewData struct {
	ID              string `json:"id"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
	DeletedAt       string `json:"deleted_at"`
	DeleteVersion   string `json:"delete_version"`
	CreateUser      string `json:"create_user"`
	UpdateUser      string `json:"update_user"`
	DeleteUser      string `json:"delete_user"`
	Name            string `json:"name"`
	Code            string `json:"code"`
	CustomsPortCode string `json:"customs_port_code"`
	Type            string `json:"type"`
	Upu             string `json:"upu"`
	Upu1            string `json:"upu1"`
	Upu2            string `json:"upu2"`
	CountryCode     string `json:"country_code"`
	Address         string `json:"address"`
	ZipCode         string `json:"zip_code"`
	Description     string `json:"description"`
	AddressSecond   string `json:"address_second"`
	ICAO            string `json:"icao"`
	TimeZoneCode    string `json:"time_zone_code"`
}

var globalSQLDB *sql.DB
var globalSQLiteDBAttend *sql.DB

func getSQLDB() *sql.DB {
	var err error
	globalSQLDB, err = sql.Open("mysql", "laxbw01:laxbw@0213!@tcp(192.168.63.220:3306)/mnl")
	if err != nil {
		panic(err)
	}

	err = globalSQLDB.Ping()
	if err != nil {
		panic(err)
	}

	//fmt.Println("\nConnected to the SQL database!\n")
	return globalSQLDB
}

func PortInfo(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Probing")

	db := getSQLDB()

	selectQuery := "SELECT Id, Type, Name, Code, ICAO, Customs_Port_Code, Country_Code, Time_Zone_Code FROM common_base_port_info"
	rows, err := db.Query(selectQuery)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var views []PortData

	for rows.Next() {
		var view PortData
		var (
			idNull, typeNull, nameNull, codeNull, icaoNull, customsPortCodeNull, countryCodeNull, timeZoneCodeNull sql.NullString
		)
		err := rows.Scan(&idNull, &typeNull, &nameNull, &codeNull, &icaoNull, &customsPortCodeNull, &countryCodeNull, &timeZoneCodeNull)
		if err != nil {
			log.Fatal(err)
		}

		// Check if the value is NULL and handle it accordingly
		if idNull.Valid {
			view.Ids = idNull.String
		}
		if typeNull.Valid {
			view.Type = typeNull.String
		}
		if nameNull.Valid {
			view.Name = nameNull.String
		}
		if codeNull.Valid {
			view.Code = codeNull.String
		}
		if icaoNull.Valid {
			view.ICAO = icaoNull.String
		}
		if customsPortCodeNull.Valid {
			view.Customs_Port_Code = customsPortCodeNull.String
		}
		if countryCodeNull.Valid {
			view.Country_Code = countryCodeNull.String
		}
		if timeZoneCodeNull.Valid {
			view.Time_Zone_Code = timeZoneCodeNull.String
		}

		views = append(views, view)
	}

	tmpl, err := template.ParseFiles("static/HTML/Port_page.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, views)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func PortCreate(w http.ResponseWriter, r *http.Request) {
	db := getSQLDB()
	fmt.Println("Probing Adding Port")
	var entryData PortEntryData

	// entryData.ID = generateUniqueID(db)
	// entryData.ID = 8542253
	// fmt.Println("Id: ", entryData.ID)

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&entryData); err != nil {
		http.Error(w, "Invalid JSON", http.StatusBadRequest)
		return
	}

	entryData.CreateUser = "93789089710878720"
	entryData.UpdateUser = "93789089710878720"

	// Use a channel to capture the result of the database operation
	resultChan := make(chan error)

	// Launch a goroutine to perform the database insertion
	go func() {
		_, err := db.Exec("INSERT INTO base_port_info (id, "+
			"type, "+
			"name, "+
			"code, "+
			"icao, "+
			"create_user, "+
			"update_user, "+
			"customs_port_code, "+
			"country_code, "+
			"time_zone_code) "+
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
			entryData.ID,
			entryData.Type,
			entryData.Name,
			entryData.Iata,
			entryData.Icao,
			entryData.CreateUser,
			entryData.UpdateUser,
			entryData.CustomsPortCode,
			entryData.CountryCode,
			entryData.TimeZoneCode)
		resultChan <- err
	}()

	go func() {
		if err := <-resultChan; err != nil {
			http.Error(w, "Database insert error", http.StatusInternalServerError)
			return
		}

		fmt.Printf("Received entry: %v\n", entryData)

		response := map[string]string{"message": "Entry added successfully"}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(response)
	}()
}

/*
	func generateUniqueID(db *sql.DB) int {
		for {

			id := rand.Int()

			var count int
			err := db.QueryRow("SELECT COUNT(*) FROM base_port_info WHERE id = ?", id).Scan(&count)
			if err != nil {
				log.Fatal(err)
			}

			if count == 0 {
				return id
			}
		}
	}
*/
func PortView(w http.ResponseWriter, r *http.Request) {
	db := getSQLDB()
	defer db.Close()

	idStr := r.URL.Query().Get("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid ID parameter", http.StatusBadRequest)
		return
	}

	//fmt.Printf("ID: %v", idStr)

	var result PortViewData
	var (
		idNull,
		createdAtNull,
		updatedAtNull,
		deletedAtNull,
		deleteVersionNull,
		createUserNull,
		updateUserNull,
		deleteUserNull,
		nameNull,
		codeNull,
		customsPortCodeNull,
		typeNull,
		upuNull,
		upu1Null,
		upu2Null,
		countryCodeNull,
		addressNull,
		zipCodeNull,
		descriptionNull,
		addressSecondNull,
		icaoNull,
		timeZoneCodeNull sql.NullString
	)

	// Define the SQL query
	query := "SELECT " +
		"id," +
		"created_at," +
		"updated_at," +
		"deleted_at," +
		"delete_version, " +
		"create_user," +
		"update_user," +
		"delete_user, " +
		"name," +
		"code," +
		"customs_port_code, " +
		"type, " +
		"upu, " +
		"upu1, " +
		"upu2," +
		"country_code," +
		"address, " +
		"zip_code, " +
		"description, " +
		"address_second, " +
		"icao, " +
		"time_zone_code " +
		"FROM base_port_info WHERE id = ?"

	err = db.QueryRow(query, id).Scan(&idNull,
		&createdAtNull,
		&updatedAtNull,
		&deletedAtNull,
		&deleteVersionNull,
		&createUserNull,
		&updateUserNull,
		&deleteUserNull,
		&nameNull,
		&codeNull,
		&customsPortCodeNull,
		&typeNull,
		&upuNull,
		&upu1Null,
		&upu2Null,
		&countryCodeNull,
		&addressNull,
		&zipCodeNull,
		&descriptionNull,
		&addressSecondNull,
		&icaoNull,
		&timeZoneCodeNull)

	if err != nil {
		if err == sql.ErrNoRows {
			http.Error(w, "Record not found", http.StatusNotFound)
			return
		}
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("Database query error: %v", err)
		return
	}

	result.ID = scanNullableString(idNull)
	result.CreatedAt = scanNullableString(createdAtNull)
	result.UpdatedAt = scanNullableString(updatedAtNull)
	result.DeletedAt = scanNullableString(deletedAtNull)
	result.DeleteVersion = scanNullableString(deleteVersionNull)
	result.CreateUser = scanNullableString(createUserNull)
	result.UpdateUser = scanNullableString(updateUserNull)
	result.DeleteUser = scanNullableString(deleteUserNull)
	result.Name = scanNullableString(nameNull)
	result.Code = scanNullableString(codeNull)
	result.CustomsPortCode = scanNullableString(customsPortCodeNull)
	result.Type = scanNullableString(typeNull)
	result.Upu = scanNullableString(upuNull)
	result.Upu1 = scanNullableString(upu1Null)
	result.Upu2 = scanNullableString(upu2Null)
	result.CountryCode = scanNullableString(countryCodeNull)
	result.Address = scanNullableString(addressNull)
	result.ZipCode = scanNullableString(zipCodeNull)
	result.Description = scanNullableString(descriptionNull)
	result.AddressSecond = scanNullableString(addressSecondNull)
	result.ICAO = scanNullableString(icaoNull)
	result.TimeZoneCode = scanNullableString(timeZoneCodeNull)

	jsonResponse, err := json.Marshal(result)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("JSON marshaling error: %v", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)

	formattedJSON, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Printf("JSON formatting error: %v", err)
		return
	}

	fmt.Printf("Data: %s\n", formattedJSON)
}

func scanNullableString(nullable sql.NullString) string {
	if nullable.Valid {
		return nullable.String
	}
	return "." // Return a dot for NULL values
}
