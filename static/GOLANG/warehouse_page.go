package controller

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
)

type WarehouseData struct {
	Code           string
	Country        string
	Name           string
	City           string
	Address        string
	Contacts       string
	Contact_number string
}

/*
func getSQLiteDB2() *sql.DB {

	var err error
	globalSQLiteDBAttend, err = sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		panic(err)
	}

	err = globalSQLiteDBAttend.Ping()
	if err != nil {
		panic(err)
	}

	return globalSQLiteDBAttend
}
*/

func WarehouseInfo(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Probing warehouse")

	db := getSQLDB()

	selectQuery := "SELECT Code, Country, Name, City, Address, Contacts, Contact_number FROM station_warehouses"
	rows, err := db.Query(selectQuery)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var views []WarehouseData

	for rows.Next() {
		var view WarehouseData
		var (
			codeNull, countryNull, nameNull, cityNull, addressNull, contactsNull, contact_numberNull sql.NullString
		)
		err := rows.Scan(&codeNull, &countryNull, &nameNull, &cityNull, &addressNull, &contactsNull, &contact_numberNull)
		if err != nil {
			log.Fatal(err)
		}

		// Check if the value is NULL and handle it accordingly
		if codeNull.Valid {
			view.Code = codeNull.String
		}
		if countryNull.Valid {
			view.Country = countryNull.String
		}
		if nameNull.Valid {
			view.Name = nameNull.String
		}
		if cityNull.Valid {
			view.City = cityNull.String
		}
		if addressNull.Valid {
			view.Address = addressNull.String
		}
		if contactsNull.Valid {
			view.Contacts = contactsNull.String
		}
		if contact_numberNull.Valid {
			view.Contact_number = contact_numberNull.String
		}

		views = append(views, view)
	}

	tmpl, err := template.ParseFiles("static/HTML/Warehouse_page.html")

	if err != nil {
		log.Println("Failed to parse template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, views)
	if err != nil {
		log.Println("Failed to execute template:", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}
