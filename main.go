package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "modernc.org/sqlite"
	controller "myproject/static/GOLANG"
	"net/http"
)

var globalSQLiteDB *sql.DB

func getSQLiteDB() *sql.DB {

	var err error
	globalSQLiteDB, err = sql.Open("sqlite", "PROFILE.db")
	if err != nil {
		panic(err)
	}

	err = globalSQLiteDB.Ping()
	if err != nil {
		panic(err)
	}

	return globalSQLiteDB
}

func main() {

	//http.HandleFunc("/generateOTP", generateOTP)
	//http.Handle("/", http.FileServer(http.Dir("static/HTML/OTP_page.html"))) // Serve static files (e.g., index.html)

	r := mux.NewRouter()
	staticDir := "/static/"
	r.PathPrefix(staticDir).Handler(http.StripPrefix(staticDir, http.FileServer(http.Dir("static"))))

	r.HandleFunc("/basicpage", controller.BasicPage).Methods("GET")

	// PORT FUNCTION
	r.HandleFunc("/port", controller.PortInfo).Methods("GET")
	r.HandleFunc("/port-create", controller.PortCreate).Methods("POST")
	r.HandleFunc("/port-view", controller.PortView).Methods("GET")

	r.HandleFunc("/carrier", controller.CarrierInfo).Methods("GET")
	r.HandleFunc("/awb_address", controller.AWB_Address_Info).Methods("GET")
	r.HandleFunc("/warehouseinfo", controller.WarehouseInfo).Methods("GET")

	//Sample D3 Javascript
	r.HandleFunc("/d3_chart", controller.D3ChartInfo).Methods("GET")

	r.HandleFunc("/generateOTP", controller.GenerateOTP).Methods("GET")

	http.Handle("/", r)
	fmt.Println("Server listening on port 8000...")
	http.ListenAndServe(":8000", nil)
}

//sample1()
//sample2()
//sample3()
/*
	//encodedStr := []byte("44YSXNEZRJXVBZLUXOXFUMRBR3CT4EPF")
	encodedStr := "44YSXNEZRJXVBZLUXOXFUMRBR3CT4EPF"

	//base32Encoded := base32.StdEncoding.EncodeToString(encodedStr)

	decodedSecretKey, err := base32.StdEncoding.DecodeString(encodedStr)
	if err != nil {
		fmt.Println("Error decoding secret key:", err)
		os.Exit(1)
	}
	fmt.Println("Original String: \n", encodedStr)
	fmt.Println("Base32 Encoded: \n", decodedSecretKey)

	//fmt.Printf("Encoded String: %v\n", encodedStr)
	issuer := "NetChb"
	accountName := "3790LaxBW"

	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      issuer,
		AccountName: accountName,
		Secret:      decodedSecretKey,
		Period:      300,
		Digits:      6,
		//Algorithm:   otp2.AlgorithmSHA1,
		Algorithm: otp2.AlgorithmSHA512,
	})
	if err != nil {
		fmt.Println("Error generating TOTP:", err)
		os.Exit(1)
	}

	// Generate the
	otp, err := totp.GenerateCode(key.Secret(), time.Now())
	if err != nil {
		fmt.Println("Error generating OTP:", err)
		os.Exit(1)
	}

	fmt.Println("Generated OTP:", otp)

	otpURI := key.URL()

	fmt.Println("OTP URI:", otpURI)

	qrCode, err := qrcode.New(otpURI, qrcode.Medium)
	if err != nil {
		fmt.Println("Error generating QR code:", err)
		os.Exit(1)
	}

	err = qrCode.WriteFile(256, "generateOTP.png")
	if err != nil {
		fmt.Println("Error saving QR code:", err)
		os.Exit(1)
	}

	fmt.Println("QR code saved as 'generateOTP.png'")
}
*/
/*

func sample1() {

	usr, err := user.Current()
	if err != nil {
		fmt.Println("Error fetching user directory:", err)
		return
	}

	excelFileName := usr.HomeDir + "/Documents/t8862.xlsx"

	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Println("Error opening Excel file:", err)
		return
	}

	firstSheet := xlFile.Sheets[0]

	for rowIndex, row := range firstSheet.Rows {
		for colIndex, cell := range row.Cells {

			cellValue := cell.String()

			if strings.TrimSpace(cellValue) == "" {
				cell.SetString("TO BE FILLED")
			} else if cellValue == "toys" {
				cell.SetString("Laruan")
			} else if cellValue == "essential oil" {
				cell.SetString("Oils")
			} else if cellValue == "260" {
				cell.SetString("Edited 260")
			}

			fmt.Printf("Row %d, Col %d: %s\n", rowIndex+1, colIndex+1, cell.String())
		}
	}

	err = xlFile.Save(usr.HomeDir + "/Documents/t86_modi.xlsx")
	if err != nil {
		fmt.Println("Error saving modified Excel file:", err)
		return
	}

	fmt.Println("Modified Excel file created:", usr.HomeDir+"/Documents/modified_sample.xlsx")

}

func sample2() {

	usr, err := user.Current()
	if err != nil {
		fmt.Println("Error fetching user directory:", err)
		return
	}

	excelFileName := usr.HomeDir + "/Documents/sample.xlsx"

	f, err := excelize.OpenFile(excelFileName)
	if err != nil {
		fmt.Println(err)
		return
	}

	cellValue := f.GetCellValue("Sheet1", "C3")

	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cellValue)

	rows := f.GetRows("Sheet1")
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, row := range rows {
		for _, colCell := range row {
			fmt.Print(colCell, "\t")
		}

		fmt.Println()
	}

	//fmt.Println(f)
}
*/
/*
func sample3() {
	db, err := sql.Open("sqlite", "SQLite_TAMS.db")
	if err != nil {
		fmt.Println("Error opening the database:", err)
		return
	}
	defer db.Close()

	replacements, err := fetchReplacementsFromDB(db)
	if err != nil {
		fmt.Println("Error fetching replacements from the database:", err)
		return
	}

	inputText, err := readFromExcel("t86.xlsx")
	if err != nil {
		fmt.Println("Error reading data from Excel:", err)
		return
	}

	words := strings.Fields(inputText)

	for i, word := range words {
		if replacement, ok := replacements[word]; ok {
			words[i] = replacement
		}
	}

	modifiedText := strings.Join(words, " ")

	err = saveToCSV("modified_output.csv", modifiedText)
	if err != nil {
		fmt.Println("Error saving data to CSV:", err)
		return
	}

	fmt.Println("Modified text saved to modified_output.csv")
}
func fetchReplacementsFromDB(db *sql.DB) (map[string]string, error) {
	replacements := make(map[string]string)

	rows, err := db.Query("SELECT target_word, replacement_word FROM table_HTS")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var targetWord, replacementWord string
		err := rows.Scan(&targetWord, &replacementWord)
		if err != nil {
			return nil, err
		}
		replacements[targetWord] = replacementWord
	}

	return replacements, nil
}

func readFromExcel(filePath string) (string, error) {
	xlFile, err := xlsx.OpenFile(filePath)
	if err != nil {
		return "", err
	}

	var inputText string

	for _, sheet := range xlFile.Sheets {
		for _, row := range sheet.Rows {
			for _, cell := range row.Cells {
				inputText += cell.String() + " "
			}
		}
	}

	return strings.TrimSpace(inputText), nil
}

func saveToCSV(outputFilePath, modifiedText string) error {
	file, err := os.Create(outputFilePath)
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	record := []string{modifiedText}
	if err := writer.Write(record); err != nil {
		return err
	}

	return nil
}
*/
